﻿using FootBall.Data;
using FootBall.Data.Model;
using System.Collections.Generic;
using System.Linq;

namespace FootBall.Core
{
    public class SearchClub
    {
        private readonly IFileSystem _fileSystem;
        private readonly IEnumerable<Club> _clubs;

        public SearchClub(IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;

            var lines = _fileSystem.ReadAllLines().Where(Utility.IsValidLine);

            _clubs = lines.ConvertToClubs();
        }

        public Club GetSmallestGoalDifference()
        {
            if (_clubs == null) return default;
            return _clubs.OrderBy(_ => _.GoalDifference).FirstOrDefault();
        }
    }
}