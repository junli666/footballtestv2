﻿using FootBall.Data;
using Moq;

namespace FootBall.Core.UnitTests.Helper
{
    public class MockFileSystem : Mock<IFileSystem>
    {
        public void MockReadAllLines(string[] output)
        {
            Setup(x => x.ReadAllLines()).Returns(output);
        }
    }
}