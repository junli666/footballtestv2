﻿using FootBall.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FootBall.Core.UnitTests
{
    [TestClass]
    public class UtilityValidatorTests
    {
        [TestMethod]
        public void GivenTokenIsLessThan10_WhenValidateLine_ThenReturnFalse()
        {
            // Arrange
            var line = "-------------------------------------------------------";

            // Act
            var sut = Utility.IsValidLine(line);

            // Assert
            Assert.IsFalse(sut);
        }

        [TestMethod]
        public void Given3edTokenIsNotInt_WhenValidateLine_ThenReturnFalse()
        {
            // Arrange
            var line = "9. Tottenham       aa    14   8  16    49  -  53    50";

            // Act
            var result = Utility.IsValidLine(line);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Given4thTokenIsNotInt_WhenValidateLine_ThenReturnFalse()
        {
            // Arrange
            var line = "9. Tottenham       11    aa   8  16    49  -  53    50";

            // Act
            var result = Utility.IsValidLine(line);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Given5thTokenIsNotInt_WhenValidateLine_ThenReturnFalse()
        {
            // Arrange
            var line = "9. Tottenham       11    14   q  16    49  -  53    50";

            // Act
            var result = Utility.IsValidLine(line);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Given6thTokenIsNotInt_WhenValidateLine_ThenReturnFalse()
        {
            // Arrange
            var line = "9. Tottenham       11    14   8  aa    49  -  53    50";

            // Act
            var result = Utility.IsValidLine(line);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Given7thTokenIsNotInt_WhenValidateLine_ThenReturnFalse()
        {
            // Arrange
            var line = "9. Tottenham       11    14   8  16    aa  -  53    50";

            // Act
            var result = Utility.IsValidLine(line);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Given9thTokenIsNotInt_WhenValidateLine_ThenReturnFalse()
        {
            // Arrange
            var line = "9. Tottenham       11    14   8  16    49  -  aa    50";

            // Act
            var result = Utility.IsValidLine(line);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Given10thTokenIsNotInt_WhenValidateLine_ThenReturnFalse()
        {
            // Arrange
            var line = "9. Tottenham       11    14   8  16    49  -  53    aa";

            // Act
            var result = Utility.IsValidLine(line);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GivenValidTokenIsPassed_WhenValidateLine_ThenReturnTrue()
        {
            // Arrange
            var line = "9. Tottenham       11    14   8  16    49  -  53    45";

            // Act
            var result = Utility.IsValidLine(line);

            // Assert
            Assert.IsTrue(result);
        }
    }
}