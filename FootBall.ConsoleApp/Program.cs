﻿using FootBall.Core;
using FootBall.Data;
using SimpleInjector;

namespace FootBall.ConsoleApp
{
    internal class Program
    {
        private static readonly Container container;

        static Program()
        {
            // setup DI
            container = new Container();
            container.Register<IFileSystem>(() => new FileSystem("footBall.dat"));
            container.Verify();
        }

        private static void Main()
        {
            // get instance from service collection
            var fileSystem = container.GetInstance<IFileSystem>();

            // instantiate a SearchClub object
            var service = new SearchClub(fileSystem);

            // get smallest goal difference (main logic)
            var result = service.GetSmallestGoalDifference();

            // print out
            System.Console.WriteLine(result);
            System.Console.ReadLine();
        }
    }
}