﻿using FootBall.Data.Model;
using System;
using System.Collections.Generic;

namespace FootBall.Data
{
    public static class Utility
    {
        public static IEnumerable<Club> ConvertToClubs(this IEnumerable<string> lines)
        {
            foreach (var line in lines)
            {
                var columns = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                yield return new Club
                {
                    ClubName = columns[1],
                    Play = int.Parse(columns[2]),
                    Win = int.Parse(columns[3]),
                    Lose = int.Parse(columns[4]),
                    Draw = int.Parse(columns[5]),
                    GoalFor = int.Parse(columns[6]),
                    GoalAgainst = int.Parse(columns[8]),
                    Points = int.Parse(columns[9])
                };
            }
        }

        public static bool IsValidLine(string line)
        {
            var lineToken = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (lineToken.Length < 10) return false;

            if (lineToken[2].IsInt()
                 && lineToken[3].IsInt()
                 && lineToken[4].IsInt()
                 && lineToken[5].IsInt()
                 && lineToken[6].IsInt()
                 && lineToken[8].IsInt()
                 && lineToken[9].IsInt())
            {
                return true;
            }
            return false;
        }

        public static bool IsInt(this string item)
        {
            var placeHolder = 0;
            return int.TryParse(item, out placeHolder);

        }
    }
}