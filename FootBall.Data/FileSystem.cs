﻿using System.IO;

namespace FootBall.Data
{
    public class FileSystem : IFileSystem
    {
        private readonly string _path;

        public FileSystem(string path)
        {
            _path = path;
        }

        public string[] ReadAllLines()
        {
            return File.ReadAllLines(_path);
        }
    }
}