﻿namespace FootBall.Data
{
    public interface IFileSystem
    {
        string[] ReadAllLines();
    }
}